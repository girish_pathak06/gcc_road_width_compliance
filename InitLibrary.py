# setup list of directories to add to the sys.path variable
import os, sys

try:
    currentDir = os.path.dirname(os.path.abspath(__file__))
    packagesDir = os.path.join(currentDir, 'packages')

    if os.path.exists(packagesDir):
        head = packagesDir

    else:
        head = os.path.join(currentDir, '..')
        
    for d in os.listdir(head):
        if not any(x for x in sys.path if x == d):
            sys.path.append(os.path.join(head, d))

except:
    raise