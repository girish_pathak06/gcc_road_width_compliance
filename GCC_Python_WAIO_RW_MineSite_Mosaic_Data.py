import InitLibrary
import shutil, datetime
import arcpy
import os,sys, json, traceback, logging, re
from optparse import OptionParser
from jenkinsapi.utils.kerberos_jenkins_requester import KerberosJenkinsRequester
from jenkinsapi.jenkins import Jenkins

class MineSiteMosaicData:
    def __init__(self, params, config):
        """Initialize all the variable and features classes with there parameters"""
        self.params = params
        # Parameters from user inputs 
        self.mineSite, self.roadType, self.bufferSize = params.mineSite.split('-')[1].strip(), params.roadType, params.bufferSize        
        
        self.surface__DEM, self.Road_SDE, self.ENV_DB, self.mineCode = config["Surface__DEM"], config["Road_SDE"], config["ENV_DB"], config["siteMapping"]
        self.center_line, self.DEM = config["Center_Line"], config["DEM"]
        
        self.Jenkins_config = config["Jenkins_config"]        
        self.output_path = config["output_path"]
        
        self.buffer_collection = config["buffer_path_collection"]
        self.full_path = self.buffer_collection[0]["out_feature"]
        
        self.DEM_lyr = config["DEM_Temp"].split(";")[3]
        self.temp_road_lyr = config["Feature_Temp"].split(";")[4]
        
        current_date = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")        
        self.output_fgdb = "{0}_RoadWidthCompliance_{1}_{2}".format(self.mineCode[self.mineSite], self.roadType ,current_date)
        
        """Create output path folder and File Geodatabase."""
        arcpy.CreateFileGDB_management(self.output_path, self.output_fgdb)
        self.workspace = "{0}\\{1}.gdb".format(self.output_path, self.output_fgdb)
        arcpy.env.overwriteOutput = True
        arcpy.env.workspace = self.workspace
        print("MineSite:{0}, RoadType: {1}".format(self.mineSite, self.roadType))
        self.extract_dem_aoi()
        
    
    def extract_dem_aoi(self):
        ## Filter Road based on Road Usage
        road_query = "RoadType = 'Haul' AND MineSite = '{0}' AND RoadUsage IN( 'SME' , 'LV_SME' )".format(self.mineSite, self.mineSite) if self.roadType == 'SME' else "RoadType IN('Minor', 'Major', 'Trucks') AND MineSite = '{0}' AND RoadUsage = 'LV'".format(self.mineSite)               
        # Process: Make Feature Layer
        arcpy.MakeFeatureLayer_management(self.Road_SDE, self.temp_road_lyr, road_query, "", "")
        # Process: Copy Features
        arcpy.CopyFeatures_management(self.temp_road_lyr, self.center_line)
        # Process: Delete Temparary Layer
        arcpy.Delete_management(self.temp_road_lyr)
        ## Create Buffer layer to filter the area around the center line with user define buffer size
        self.clip_buffer_area()
        ## FIlter the area Mine Site Area from SDE         
        arcpy.MakeMosaicLayer_management(self.surface__DEM, self.DEM_lyr, "Name = 'DEM_1m'", self.surface__DEM, "", "", "AcquisitionDate", "2080/12/30", "", "ASCENDING", "FIRST", "1", "None")
        arcpy.Clip_management(in_raster=self.DEM_lyr, rectangle="#", out_raster=self.DEM, in_template_dataset=self.full_path,  clipping_geometry="ClippingGeometry", maintain_clipping_extent="MAINTAIN_EXTENT")
        arcpy.Delete_management(self.DEM_lyr)
        ## Call Jenkin job for Windrows height calculation
        self.__invokeJenkins()
        
    
    def clip_buffer_area(self):       
        for buffer in self.buffer_collection:
            buffer_size = buffer["buffer_distance_or_field"] if not buffer["buffer_distance_or_field"] == "" else "{0} Meters".format(float(self.bufferSize)/2)
            # Buffer for area of interest based on center line
            arcpy.Buffer_analysis(in_features=self.center_line, out_feature_class=buffer["out_feature"], buffer_distance_or_field=buffer_size, line_side=buffer["line_side"], line_end_type=buffer["line_end_type"], dissolve_option=buffer["dissolve_option"], method=buffer["method"])
            

    def __initJenkins(self):
        print("Jenking Object initialize")
        kerberosJenkinsRequester = KerberosJenkinsRequester()
        # Initializing the jenkin object with configuration
        self.jenkins = Jenkins(kerberosJenkinsRequester.getDNS_A_Url(self.Jenkins_config["jenkins_base_url"].encode('utf-8')), requester=kerberosJenkinsRequester)

    def __invokeJenkins(self):
        self.__initJenkins()
        parameter = vars(self.params)
        parameter.update({'fgdb' : self.workspace} )
        print(parameter)
        jenkin_job = self.jenkins.get_job(self.Jenkins_config["job_road_width"].encode('utf-8'))
        # Calling the Road Width job
        jenkin_job.invoke(build_params=parameter)
    

def main():
    parser = OptionParser()
    parser.add_option("-u", "--mineSite", action="store", dest="mineSite", type="string", help="Mine Site Names") 
    parser.add_option("-a", "--bufferSize", action="store", dest="bufferSize", type="string", help="Buffer Size Along the Center Line") 
    parser.add_option("-f", "--roadType", action="store", dest="roadType", type="string", help="Road Types ie SME, LV")
    parser.add_option("-e", "--emailList", action="store", dest="emailList", type="string", help="Email List of users for sending notification")
    parser.add_option("-p", "--emailParams", action="store", dest="emailParams", type="string", help="Email Parameters of users for sending notification")
    parser.add_option("-c", "--configFolder", action="store", dest="configFolder", type="string", help="Path to the JSON configuration file")
    parser.add_option("-l", "--level", action="store", dest="level", type="int", default=1, help="Levels to search for JSON config files") 
    (__options, args) = parser.parse_args()
    try:
        import GCC_Python_Config as c
        configCls = c.Config()
        global __config
        __config = configCls.GetConfig(folder=__options.configFolder, level=__options.level)
        print(__options.emailParams)
        mineSite_mosaic_data = MineSiteMosaicData(__options, __config)
    except:
        print(traceback.format_exc())
        sys.exit(1)        

if __name__ == '__main__':
    # get root directory for code...
    global scriptlocation
    scriptfile = sys.argv[0]
    if not os.path.dirname(scriptfile):
        scriptfile = os.path.join(os.getcwd(), sys.argv[0])
    scriptlocation, scriptname = os.path.split(scriptfile)
    main()