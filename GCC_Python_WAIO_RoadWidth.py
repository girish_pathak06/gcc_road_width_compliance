# coding: utf-8
import InitLibrary
import time, json
import arcpy
from arcpy.sa import *
import arcgisscripting
import os,sys, json, traceback, logging
from optparse import OptionParser
import datetime

class RoadWidth:
    def __init__(self, params, config, email):
        """Initialize all the variable and features classes with there parameters"""
        self.params = params
        self.email = email
        # Parameters from user inputs 
        self.mineSite, self.roadType, self.bufferSize, self.workspace = params.mineSite.split('-')[1].strip(), params.roadType, params.bufferSize, params.fgdb
        self.mineCode = params.mineSite.split('-')[0].strip()
        self.final_gdb_path = config["final_gdb_path"]
        self.zonal_buffer_size = config["zonal_buffer_size"]
        self.compliance, self.non_compliance = 0.0, 0.0
        self.fgdb_name_list = self.workspace.split("\\")[-1:][0].split("_")
        self.surface__DEM, self.Road_SDE, self.RoadsWidthSDE = config["Surface__DEM"], config["Road_SDE"], config["RoadsWidth_SDE"]
        self.RoadWidthMosaic, self.projection = config["RoadWidthMosaic"], config["projection"]["MGA50"]
        self.field_mapping_array, self.status_field_value = config["field_mapping_array"], config["status_field_value"]
        self.center_line, self.DEM, self.shape_area_count  = config["Center_Line"], config["DEM"], config["shape_area_count"]
        self.road_width = config["road_width"]
        self.output_path = config["output_path"]
        self.email_params = config["email_params"]
        self.email_params["send_to"] = [params.emailList]      
        self.buffer_collection, self.ENV_DB = config["buffer_path_collection"],  config["ENV_DB"]        
        self.full_path, self.road_width_transect = self.buffer_collection[0]["out_feature"], config["road_width_transect"]
        self.width_compliance_value = config["width_compliance_value"]
        self.Jenkins_config = config["Jenkins_config"]
        self.reclassify_ranges = config["reclassifyRanges"]        
        self.road_width_field = config["road_width_field"]
        self.transect_length = "{0} Meters".format(self.bufferSize)       
        
        
        self.DEM_Temp = config["DEM_Temp"].split(";")
        self.feature_temp = config["Feature_Temp"].split(";")
        self.raster_to_polygon = self.feature_temp[0]
        # Creating all working feature class in in_memory if config in_memory is true, otherwise in temporary File Geodatabase
        if config["in_mem"] == "True":
            self.fc_in_memory()
        else:
            self.fc_fgdb_init()
        self.road_width_feature = "{0}\\{1}_RoadWidthCompliance_{2}_{3}_{4}".format(self.final_gdb_path, self.fgdb_name_list[0], self.roadType, self.fgdb_name_list[3], self.fgdb_name_list[4].split('.')[0])
        self.rw_attributes_value = config["rw_attributes_value"]
        self.zonal_field = config["zonal_field"]
        
        self.part_area = config["area_part_removal"] 
        self.road_edges = config["road_edges"]
        """Create output path folder and File Geodatabase."""  
        arcpy.env.overwriteOutput = True
        arcpy.env.workspace = self.workspace
        self.accessible_area()
        
        
    def fc_in_memory(self):
        self.DEM_focalStats = self.init_in_memory(self.DEM_Temp, 0)
        self.DEM_curvature = self.init_in_memory(self.DEM_Temp, 1)
        self.DEM_reclassify = self.init_in_memory(self.DEM_Temp, 2)
        self.DEM_Compliance = self.init_in_memory(self.DEM_Temp, 4)
        self.DEM_NonCompliance = self.init_in_memory(self.DEM_Temp, 5)
        
        self.pathway =  self.init_in_memory(self.feature_temp, 1)    
        self.right_of_way = self.init_in_memory(self.feature_temp, 2)
        self.profile_line = self.init_in_memory(self.feature_temp, 3) 
        self.without_holes = self.init_in_memory(self.feature_temp, 5) 
        self.road_edges_LINE_Smooth = self.init_in_memory(self.feature_temp, 6) 
        self.road_edges_temp = self.init_in_memory(self.feature_temp, 7)         
        self.stat_table =  self.init_in_memory(self.feature_temp, 8) 
        self.road_width_multipart =  self.init_in_memory(self.feature_temp, 9)
        self.road_width_singlepart =  self.init_in_memory(self.feature_temp, 10) 
        self.road_width_lyr = self.init_in_memory(self.feature_temp, 11)
        self.profile_line_temp = self.init_in_memory(self.feature_temp, 12)
        self.zonal_area = self.init_in_memory(self.feature_temp, 13)
        
    def fc_fgdb_init(self):
        self.DEM_focalStats = self.DEM_Temp[0]
        self.DEM_curvature = self.DEM_Temp[1]
        self.DEM_reclassify = self.DEM_Temp[2]
        self.DEM_Compliance = self.DEM_Temp[4]
        self.DEM_NonCompliance = self.DEM_Temp[5]
        
        self.pathway =  self.feature_temp[1]
        self.right_of_way = self.feature_temp[2]
        self.profile_line = self.feature_temp[3]
        self.without_holes = self.feature_temp[5]
        self.road_edges_LINE_Smooth = self.feature_temp[6]
        self.road_edges_temp = self.feature_temp[7]
        self.stat_table =  self.feature_temp[8]
        self.road_width_multipart =  self.feature_temp[9]
        self.road_width_singlepart =  self.feature_temp[10]
        self.road_width_lyr = self.feature_temp[11]
        self.profile_line_temp = self.feature_temp[12]
        self.zonal_area = self.feature_temp[13]

        
    def init_in_memory(self, feature_lyr, index):
        return "{0}\\{1}".format(self.ENV_DB,  feature_lyr[index])

    def accessible_area(self):
        try:         
            # Check out Spatial Analyst extension license
            arcpy.gp.CheckOutExtension("Spatial")
            """ Focal Statistics (mean) on 1 Meter DEM with  3x3 meter cell neighbourhood. 
                I don't want to smooth the DTM too much, so I take a 3x3 (cell) neighborhood ."""
            # Process: FocalStatistics
            arcpy.gp.FocalStatistics_sa(self.DEM, self.DEM_focalStats, "Rectangle 3 3 CELL", "MEAN", "DATA")
            # Process Curvature : Calculate the Profile Curvature on the smoothed DTM
            arcpy.Curvature_3d(in_raster=self.DEM_focalStats, out_curvature_raster=self.DEM_curvature, z_factor="1")
            # Process: Reclassify - Reclassify the curvature raster
            arcpy.gp.Reclassify_sa(self.DEM_curvature, "VALUE", self.reclassify_ranges, self.DEM_reclassify, "DATA")            
            ## Convert ReClass from Raster to polygon
            arcpy.RasterToPolygon_conversion(in_raster=self.DEM_reclassify, out_polygon_features=self.raster_to_polygon, simplify="NO_SIMPLIFY", raster_field="Value")
            arcpy.gp.CheckInExtension("Spatial")
            self.remove_noise()
        except:
            print(arcpy.gp.GetMessages()) # If an error occurred while running a tool, then print the messages.
 
    def remove_noise(self):        
        arcpy.Statistics_analysis(self.raster_to_polygon, self.stat_table, "Shape_Area STD", "")
        cursor=arcpy.SearchCursor(self.stat_table)
        for row in cursor:
            self.shape_area_count = row.getValue("STD_Shape_Area")            
        try:
            ## Calculate the max shape value fro Raster to poly feature class to get the ROF feature class          
            query = "{0} > {1}".format("Shape_Area", int(self.shape_area_count))
            arcpy.MakeFeatureLayer_management(in_features=self.raster_to_polygon, out_layer=self.pathway, where_clause=query)
            ## Eliminating the noise from Accessible area
            arcpy.EliminatePolygonPart_management(in_features=self.pathway, out_feature_class=self.without_holes, condition="AREA", part_area=self.part_area, part_option="ANY")
            ## Converting the Accessible Area Polygon to Line feature 
            arcpy.FeatureToLine_management(in_features=self.without_holes, out_feature_class=self.road_edges_temp, cluster_tolerance="", attributes="NO_ATTRIBUTES")
            ## Simplify the Road Edges which is in zigzag 
            arcpy.SimplifyLine_cartography(in_features=self.road_edges_temp, out_feature_class=self.road_edges_LINE_Smooth, algorithm="POINT_REMOVE", tolerance="1 Meters", collapsed_point_option="NO_KEEP")            
            ## Copy the Road Edges Feature class to File GeoDatabase
            arcpy.CopyFeatures_management(in_features=self.road_edges_LINE_Smooth, out_feature_class=self.road_edges)
            if arcpy.Exists(self.stat_table):
                arcpy.Delete_management(self.stat_table)
            self.generate_profile()
            self.road_width_calculation()
        except arcpy.ExecuteError:
            print("Not able to remove the noise.")

    def generate_profile(self):
        arcpy.CheckOutExtension("Spatial")
        try:
            # Generating Profile line with 1 meter transact along the center line
            arcpy.GenerateTransectsAlongLines_management(in_features=self.center_line, out_feature_class=self.profile_line_temp, transect_length=self.transect_length, interval=self.road_width_transect)
            RE_centre_line_joined = arcpy.AddJoin_management(in_layer_or_view=self.profile_line_temp, in_field="ORIG_FID", join_table=self.center_line, join_field="OBJECTID", join_type="KEEP_COMMON")
            arcpy.CopyFeatures_management(RE_centre_line_joined, self.profile_line)
            arcpy.RemoveJoin_management(RE_centre_line_joined)
            # arcpy.DeleteField_management(in_table="", drop_field=[])
            arcpy.Clip_analysis(in_features=self.profile_line, clip_features=self.without_holes, out_feature_class=self.road_width_multipart, cluster_tolerance="1 Meters")
        except arcpy.ExecuteError:
            print("Not able to Generate the profile line.")
        arcpy.CheckInExtension("Spatial")
    
    def identify_single_dual_lane(self, design_width, road_width):
        if road_width is None:
            return "No Data"
        if self.mineCode == "JB":
            if design_width == self.width_compliance_value[self.mineCode]["design_width_dual"]: # Straight Dual lane - 
                return self.compliance_test(road_width, self.width_compliance_value[self.mineCode]["dual_lane"])
            else: # Straight Single Lane -
                return self.compliance_test(road_width, self.width_compliance_value[self.mineCode]["single_lane"])              
        else:
            if design_width == self.width_compliance_value["OTHERS"]["design_width_single"]: # Straight Single lane - 
                return self.compliance_test(road_width, self.width_compliance_value["OTHERS"]["dual_lane"])
            elif design_width == self.width_compliance_value["OTHERS"]["design_width_dual"]: # Straight Dual Lane - 
                return self.compliance_test(road_width, self.width_compliance_value["OTHERS"]["single_lane"])

    def compliance_test(self, road_width, width):
        return (lambda: self.rw_attributes_value[0], lambda: self.rw_attributes_value[1])[road_width < width]()
    
    def field_management(self):
        self.field_list = ["SHAPE@", "centerline_DesignWidth"]        
        try:
            # Field added in temporary centre line
            for fields in self.road_width_field:
                arcpy.AddField_management(in_table=self.road_width_singlepart, field_name=fields["field_name"], field_type=fields["field_type"])
                self.field_list.append(fields["field_name"])        
            print("Field::", self.field_list)
        except arcpy.ExecuteError:
            print("Could not Add the fields to Road Width Feature Class..!")
    
    def road_width_calculation(self):
        # Convert the Multipart Road Width Feature Class to Single Part Feature
        arcpy.MultipartToSinglepart_management(in_features=self.road_width_multipart, out_feature_class=self.road_width_singlepart)
        self.field_management()
        # Make a layer and select road width which overlap the road centre line
        arcpy.MakeFeatureLayer_management(in_features=self.road_width_singlepart, out_layer=self.road_width_lyr)
        arcpy.SelectLayerByLocation_management(in_layer=self.road_width_lyr, overlap_type="INTERSECT", select_features=self.center_line)
        arcpy.CopyFeatures_management(in_features=self.road_width_lyr, out_feature_class=self.road_width)
        timestamp = self.fgdb_name_list[3] + self.fgdb_name_list[4].split('.')[0]
        analysis_date = datetime.datetime.strptime(timestamp, "%Y%m%d%H%M%S")
        # Updated the atribute values in temporary road width feature class
        with arcpy.da.UpdateCursor(self.road_width, self.field_list) as cursor:
            for row in cursor:
                length = row[0].getLength("GEODESIC", "METERS")
                compliance_status = self.identify_single_dual_lane(row[1], length)
                if compliance_status == "Compliant":
                    self.compliance += 1.0
                else:
                    self.non_compliance+=1.0
                row[2] = self.fgdb_name_list[0] # Site : site name i.e. "MAC", "JB", "ER", "WB" or "YN’"
                row[3] = length # Road_Width_m : Width of the road at this section in metre                             
                row[4] = analysis_date # Analysis_Date : Date the tool was run                
                row[5] = compliance_status # Compliance : identify the road which is come under compliance and non-compliance
                cursor.updateRow(row)

        # Copy the Accessible Area & Road Edges to RoadWidth File Geodatabase      
        access_area_poly = "{0}\\{1}_RoadWidthCompliance_{2}_{3}_{4}_accessible_area".format(self.final_gdb_path, self.fgdb_name_list[0], self.roadType, self.fgdb_name_list[3], self.fgdb_name_list[4].split('.')[0])      
        road_edges_output = "{0}\\{1}_RoadWidthCompliance_{2}_{3}_{4}_road_edges".format(self.final_gdb_path, self.fgdb_name_list[0], self.roadType, self.fgdb_name_list[3], self.fgdb_name_list[4].split('.')[0])      
        
        # arcpy.CopyFeatures_management(in_features=self.road_width, out_feature_class=self.road_width_feature)
        roadwidth_name = "{0}_RoadWidthCompliance_{1}_{2}_{3}".format(self.fgdb_name_list[0], self.roadType, self.fgdb_name_list[3], self.fgdb_name_list[4].split('.')[0])
        temp_road_width_projected = self.feature_temp[15]
        # Reproject the Road Width 
        arcpy.Project_management(in_dataset=self.road_width, out_dataset=temp_road_width_projected, out_coor_system=self.projection)
        arcpy.FeatureClassToFeatureClass_conversion(in_features=temp_road_width_projected, out_path=self.final_gdb_path, out_name=roadwidth_name, where_clause="Road_Width_m >5")
        
        arcpy.CopyFeatures_management(in_features=self.raster_to_polygon, out_feature_class=access_area_poly)
        arcpy.CopyFeatures_management(in_features=self.road_edges, out_feature_class=road_edges_output)
        print("Road Width Feature class copied to Road Width gdb file.")
        self.generate_raster_output()

    def generate_raster_output(self):
        des = arcpy.Describe(value=self.DEM)
        extent = str(des.extent).split("NaN")[0].strip()
        arcpy.CheckOutExtension("Spatial")
        ## Calculating zonal area for raster output
        arcpy.Buffer_analysis(in_features=self.road_width, out_feature_class=self.zonal_area, buffer_distance_or_field=self.zonal_buffer_size, line_side="FULL", line_end_type="FLAT", dissolve_option="NONE", method="PLANAR")
        
        for attib in self.rw_attributes_value:
            where_clause = "{0} = '{1}'".format(self.zonal_field, attib)
            output_raster = "temp_DEM_{}".format(attib)
            # Select only the compliance or non-compliance records 
            compliance_lyr = arcpy.SelectLayerByAttribute_management(in_layer_or_view=self.zonal_area, selection_type="NEW_SELECTION", where_clause=where_clause)

            arcpy.Clip_management(in_raster=self.DEM, rectangle=extent, out_raster=output_raster, in_template_dataset=compliance_lyr, nodata_value="-3.402823e+038", clipping_geometry="ClippingGeometry", maintain_clipping_extent="NO_MAINTAIN_EXTENT")
            if attib == 'Compliant':
                # compliance_raster = Raster(output_raster) / Raster(output_raster)
                compliance_raster = Raster(output_raster) * 0 + 1
                compliance_raster.save(self.DEM_Compliance)
            else:
                non_compliance_raster = Raster(output_raster) * 0
                non_compliance_raster.save(self.DEM_NonCompliance)
            arcpy.Delete_management(compliance_lyr)
            # arcpy.Delete_management(output_raster)
        # Merge the Compliance DEM with Non-Compliance DEM
        arcpy.Mosaic_management(inputs=self.DEM_NonCompliance, target=self.DEM_Compliance)
        self.DEM_RW = "{0}\\{1}_RoadWidthCompliance_{2}_DEM_{3}_{4}".format(self.final_gdb_path, self.fgdb_name_list[0], self.roadType, self.fgdb_name_list[3], self.fgdb_name_list[4].split('.')[0])
        DEM_Compliance_path = "{0}\\{1}".format(self.workspace, self.DEM_Compliance)
        # Process: Copy Raster
        arcpy.CopyRaster_management(in_raster=DEM_Compliance_path, out_rasterdataset=self.DEM_RW,  nodata_value="-3.402823e+038", onebit_to_eightbit="NONE", colormap_to_RGB="ColormapToRGB", pixel_type="2_BIT", scale_pixel_value="NONE", RGB_to_Colormap="NONE", format="", transform="NONE")
        
        try:            
            temp_road_width = self.feature_temp[14]
            sql_query = "Site = '{0}'".format(self.fgdb_name_list[0])
            # Process: Make Table View
            arcpy.MakeTableView_management(in_table=self.RoadsWidthSDE, out_view=temp_road_width, where_clause=sql_query)

            if int(arcpy.GetCount_management(temp_road_width).getOutput(0)) > 0:
                arcpy.DeleteRows_management(temp_road_width) # Process: Delete Rows
                arcpy.Delete_management(temp_road_width)
            fieldmappings = self.fields_mapping()
            # Append the Road Width Feature Class to Database via SDE connection
            arcpy.Append_management(inputs=self.road_width_feature, target=self.RoadsWidthSDE, schema_type="NO_TEST", field_mapping=fieldmappings)
        except arcpy.ExecuteError:
            print("Unable to copy the data to SDE dataset!")            
        finally:
            # Cleanup
            arcpy.Delete_management(temp_road_width)        
        arcpy.CheckInExtension("Spatial")
        self.create_mosaic_dataset()
        self.send_email()
        self.cleanup()

    def fields_mapping(self):
        fms = arcpy.FieldMappings()
        for field_ar in self.field_mapping_array:
            # Create the FieldMap object
            field_map = arcpy.FieldMap()
            # Add fields to FieldMap object
            field_map.addInputField(self.RoadsWidthSDE, field_ar[1])
            field_map.addInputField(self.road_width_feature, field_ar[0])
            # Add the FieldMap objects to the FieldMappings object
            fms.addFieldMap(field_map)
        return fms
    
    def create_mosaic_dataset(self):
        field_list = ["Status", "MineSite"]
        SQLQuery = "(Status IS NULL OR Status = '{0}') AND (MineSite = '{1}' OR MineSite IS NULL)".format(self.status_field_value[0], self.mineSite)
        try:    
            # Process: Add Rasters To Mosaic Dataset
            arcpy.AddRastersToMosaicDataset_management(self.RoadWidthMosaic, "Raster Dataset", self.DEM_RW, "UPDATE_CELL_SIZES", "UPDATE_BOUNDARY", "NO_OVERVIEWS", "", "0", "1500", "", "", "SUBFOLDERS", "ALLOW_DUPLICATES", "NO_PYRAMIDS", "NO_STATISTICS", "NO_THUMBNAILS", "", "NO_FORCE_SPATIAL_REFERENCE", "NO_STATISTICS", "")

            with arcpy.da.UpdateCursor(self.RoadWidthMosaic, field_list, SQLQuery) as cursor:
                for row in cursor:
                    if row[0] is None:
                        row[0] = self.status_field_value[0]
                        row[1] = self.mineSite
                    else:
                        row[0] = self.status_field_value[1]

                    cursor.updateRow ( row )

            del cursor
        except arcpy.ExecuteError:
            print("Unable to copy the data to SDE dataset!")            
    
    def send_email(self):
        compliance_per = "{}%".format(round((self.compliance/(self.compliance + self.non_compliance))*100, 2))
        non_compliance_per = "{}%".format(round((self.non_compliance/(self.compliance + self.non_compliance))*100, 2))
        try:
            self.email_params['gcc_content'] = self.email_params['gcc_content'].replace('[[final_gdb_path]]', "self.final_gdb_path")
            self.email_params['gcc_content'] = self.email_params['gcc_content'].replace('[[roadwidth_fc]]', "self.road_width_feature")
            self.email_params['gcc_content'] = self.email_params['gcc_content'].replace('[[mineSite]]', "self.mineSite")
            self.email_params['gcc_content'] = self.email_params['gcc_content'].replace('[[compliance]]', "str(self.compliance)")
            self.email_params['gcc_content'] = self.email_params['gcc_content'].replace('[[non_compliance]]', "str(self.non_compliance)")
            self.email_params['gcc_content'] = self.email_params['gcc_content'].replace('[[compliance_per]]', "str(compliance_per)")
            self.email_params['gcc_content'] = self.email_params['gcc_content'].replace('[[non_compliance_per]]', "str(non_compliance_per)")
            self.email.send_gcc_email(config=self.email_params)        
        except:
            raise Exception(traceback.format_exc())  
            
    def cleanup(self):
        if arcpy.Exists(self.workspace):
            try:
                time.sleep(5)
                arcpy.Delete_management(self.workspace)
            except arcpy.ExecuteError:                
                print("Could not delete working file. Tried again..!")           
    


def main():
    parser = OptionParser()
    parser.add_option("-u", "--mineSite", action="store", dest="mineSite", type="string", help="Mine Site Names") 
    parser.add_option("-a", "--bufferSize", action="store", dest="bufferSize", type="string", help="Buffer Size Along the Center Line")
    parser.add_option("-s", "--fgdb", action="store", dest="fgdb", type="string", help="Mosiac File geodatabase path")
    parser.add_option("-f", "--roadType", action="store", dest="roadType", type="string", help="Road Types ie SME, LV")
    parser.add_option("-e", "--emailList", action="store", dest="emailList", type="string", help="Email List of users for sending notification")
    parser.add_option("-c", "--configFolder", action="store", dest="configFolder", type="string", help="Path to the JSON configuration file")
    parser.add_option("-l", "--level", action="store", dest="level", type="int", default=1, help="Levels to search for JSON config files")
    (__options, args) = parser.parse_args()
    try:
        import GCC_Python_Config as c
        configCls = c.Config()       
        __config = configCls.GetConfig(folder=__options.configFolder, level=__options.level)
        import GCC_Python_Logging as l
        log = l.LoggingCls(config=__config['logging'])
        import GCC_Python_Email as e
        __email = e.Email()
        windrows_height_compliance = RoadWidth(__options, __config, __email)
    except:
        print(traceback.format_exc())
        sys.exit(1)

if __name__ == '__main__':
    # get root directory for code...
    global scriptlocation
    scriptfile = sys.argv[0]
    if not os.path.dirname(scriptfile):
        scriptfile = os.path.join(os.getcwd(), sys.argv[0])
    scriptlocation, scriptname = os.path.split(scriptfile)
    main()